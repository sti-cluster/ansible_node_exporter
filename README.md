Ansible node_exporter
=========

This role will install a prometheus node_exporter (v0.18.1) on an ubuntu machine

Requirements
------------

Running on a Debian machine (64 bits) using systemd.

Role Variables
--------------

No variables available at the moment

Dependencies
------------

None

Example Playbook
----------------

```yaml
---
- hosts: all
  tasks:
    - block:
        - name: prometheus node exporter
          include_role:
            name: prometheus_node_exporter
      become: yes
```
